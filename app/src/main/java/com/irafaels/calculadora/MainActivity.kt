package com.irafaels.calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()

/*Eu adicionei ações nos textView, mas eu lembro que você falando na sala que tinha que transformar em button. Assim funcionou também
        mas acho que a minha solução não foi a mais correta*/
        findViewById<TextView>(R.id.numero_zero).setOnClickListener{AcrescentarExpressao( "0", true)}
        findViewById<TextView>(R.id.numero_um).setOnClickListener{AcrescentarExpressao( "1", true)}
        findViewById<TextView>(R.id.numero_dois).setOnClickListener{AcrescentarExpressao( "2", true)}
        findViewById<TextView>(R.id.numero_três).setOnClickListener{AcrescentarExpressao( "3", true)}
        findViewById<TextView>(R.id.numero_quatro).setOnClickListener{AcrescentarExpressao( "4", true)}
        findViewById<TextView>(R.id.numero_cinco).setOnClickListener{AcrescentarExpressao( "5", true)}
        findViewById<TextView>(R.id.numero_seis).setOnClickListener{AcrescentarExpressao( "6", true)}
        findViewById<TextView>(R.id.numero_sete).setOnClickListener{AcrescentarExpressao( "7", true)}
        findViewById<TextView>(R.id.numero_oito).setOnClickListener{AcrescentarExpressao( "8", true)}
        findViewById<TextView>(R.id.numero_nove).setOnClickListener{AcrescentarExpressao( "9", true)}

        //operadores
        findViewById<TextView>(R.id.soma).setOnClickListener{AcrescentarExpressao( "+", false)}
        findViewById<TextView>(R.id.multiplicacao).setOnClickListener{AcrescentarExpressao( "*", false)}
        findViewById<TextView>(R.id.subtracao).setOnClickListener{AcrescentarExpressao( "-", false)}
        findViewById<TextView>(R.id.divisao).setOnClickListener{AcrescentarExpressao( "/", false)}
        findViewById<TextView>(R.id.ponto).setOnClickListener{AcrescentarExpressao( ".", true)}


        //limpar quando o usuário pressiona o botão clear "C"
        findViewById<TextView>(R.id.limpar).setOnClickListener {

            findViewById<TextView>(R.id.expressao).text = ""
            findViewById<TextView>(R.id.resultado).text = ""
        }


        //apagando o que foi digitado
        findViewById<ImageView>(R.id.backespace).setOnClickListener{

            val string = findViewById<TextView>(R.id.expressao).text.toString()

            if(string.isNotBlank()){
                findViewById<TextView>(R.id.expressao).text = string.substring(0,string.length-1)
            }
            findViewById<TextView>(R.id.resultado).text = ""
        }

        findViewById<TextView>(R.id.igual).setOnClickListener{

            try{
                //ExpressionBuilder pertence à biblioteca objecthunter que foi adicionada no gradle.app
                /*Basicamente eu usei essa biblioteca para pegar o que está digitado no visor e construir uma expressão matemática.
                dessa forma é possível tratar a precedência dos operadores matemáticos.*/
                val expressao = ExpressionBuilder(findViewById<TextView>(R.id.expressao).text.toString()).build()

                val resultado = expressao.evaluate()

                val longResult = resultado.toLong()

                if(resultado == longResult.toDouble()){
                    findViewById<TextView>(R.id.resultado).text = longResult.toString()
                }else{
                    findViewById<TextView>(R.id.resultado).text = resultado.toString()
                }
            }catch (e: Exception){

            }
        }



    }

    fun AcrescentarExpressao(string: String, limpar_dados:Boolean){
        val textView_resultado = findViewById<TextView>(R.id.resultado)
        val txt_expressao = findViewById<TextView>(R.id.expressao)
        if(textView_resultado.text.isNotEmpty()){

            textView_resultado.text = ""


        }
        if(limpar_dados){
            textView_resultado.text=""
            txt_expressao.append(string)

        }else{
            txt_expressao.append(textView_resultado.text)
            txt_expressao.append(string)

        }


    }

}
